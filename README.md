# really good game.jiff

### base concept
* co-op game play
* can be played in a 45 minutes time period
* Players can play either in person or remotely. 
* Character, stats and (inventory?) persist through games
* Game boards are dynamic through each play using hexagon squares similiar to Settlers of Catan
* Encounters are scaled to match game characters.
* loot rewards, possibly shared


### character concepts
* dynamic build interface with character perks based on stat variables
* characters saved in a persistant database

### character building 
* Gender
* Orientation
* Age
* Weight
* primary stats
  * health
  * energy
* character stats(200 total), player defined
  * Strength
  * Intelligence
  * Dexterity
* character perk/unique ability
* Fatal Flaw

#### character perk concepts
* Heal Target
* Heal Party
* Revive Target
* Cure Target
* Cure Party
* Set Trap
* Break Object
* Barricade
* Attack
* Precision Strike
* Cripple
* Battle Cry
* Sneak
* Lockpick
* Detect
* Steal
* Incite
* Calm
* Confuse
* Summon Illusion
* Summon Minion
* Summon Portal
* Possess Target
* Shot Caller
* Identify
* Scout Terrain
* Escape
* Get Information
* Lie your way out
* Speak Truthfully
* Distract


